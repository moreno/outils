#  Gestion des Interfaces réseaux (sans network manager)

(prise de notes - cours de sdejongh - 2022)

Contexte : pas de network manager sur une distro en interface graphique. L'utilisateur n'ayant d'ailleurs pas à y toucher. 
On jette un oeil :     
```sudo systemctl status NetworkManager``` 

`nmcli` : changer paramètre sans conflit et sans passer par l'interface graphique.    

Stopper le network manager :     
`sudo systemctl stop NetworkManager`  

"Disable" n'est plus lancé automatiquement mais peut être lancé  manuelement :  
` sudo systemctl disable NetworkManager  ` 

Démarrage manuel : impossible car service est masqué.     
` sudo systemctl mask NetworkManager `   

Si tu réessaies :     
``` 
sudo systemctl start NetworkManager  
Failed to start NetworkManager.service:   
Unit NetworkManager.service is masked. 

```  

Démasquer : `sudo systemctl unmask NetworkManager `  
Mais il y a un autre service qui tourne :   
  
```
sudo systemctl status networking  
● networking.service - Raise network interfaces  
     Loaded: loaded (/lib/systemd/system/networking.service;   enabled; vendor preset: enabled)  
     Active: active (exited) since Thu 2022-09-15 08:49:27 BST;   15min ago  
       Docs: man:interfaces(5)  
   Main PID: 487 (code=exited, status=0/SUCCESS)  
      Tasks: 0 (limit: 9477)  
     Memory: 0B  
        CPU: 0  
     CGroup: /system.slice/networking.service  

Sep 15 08:49:27 debian-base systemd[1]: Starting Raise network interfaces...  
Sep 15 08:49:27 debian-base systemd[1]: Finished Raise network interfaces.  

```  

## Comment interragir avec le service networking ?     
` ip -V `        
` ip  utility` : permet de gerer la partie interface virtuelle, les   créer, les supprimer, addressage logique, routable...  les config ne sont pas permanentes. Elles sont "direct" mais, après   sessions, elles ne seront plus là.       

` sudo vim /etc/network/interfaces` 

En effet, la commande `ip` : (voir OBJECT dans le man) est non persistante.  

Voir les informations de l'interface :     
`ip link ` : information bas niveau  
`ip link show nomdel'interface (exemple enp0s3)`  
permet d'activer ou désactiver une interface.

`ip link set (up | down)`: changement d'état d'interface  
ou `ip link protodown on | off` (?)  

L'interface est donc "down" :   
```  
ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff 

```  

 ` sudo ip link set enp0s3 up` active l'interface :  
  
``` 
ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state   UNKNOWN mode DEFAULT group default qlen 1000    
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00    
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP mode DEFAULT group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  

```

Elle est fonctionnelle mais inactive :
``` 
sudo ifquery --list   
 
lo   
ici, elle est down : sudo ip link set enp0s3 down  
```    
Mais ce n'est pas en soi la bonne manière d'ouvrir ou fermer, allumer une interface. 


## Configuration :   
Note : réseau pour la future interface = 10.255.2.0/24   
Dans le but de communiquer sur le réseau, on attribue une   addresse une l'interface.   

` ip address` : pour l'addressage ipv4 et ipV6  

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state   UNKNOWN group default qlen 1000  
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
    inet 127.0.0.1/8 scope host lo  
       valid_lft forever preferred_lft forever  
    inet6 ::1/128 scope host   
       valid_lft forever preferred_lft forever  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link   
       valid_lft forever preferred_lft forever 
```

Note: la loopback n'est pas routable, permet de communiquer dans le réseau local = `scope host lo  ` 

Voir ip address (add |change|replace)       
`ip address del`: delete une addresse    
`ip address save` :(faire un dump)| flush (tirer la chasse)     
`ip address show` : affiche    
`ip address` : (showdump |restore)     

```
ip add show enp0s3  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link   
       valid_lft forever preferred_lft forever 
```  

On souhaite configurer une nouvelle adresse sur le réseau   10.255.2.0/24:    
`sudo ip address add 10.255.2.100/24 dev enp0s3  `  
On vérifie :   
``` 
 ~$ ip add show enp0s3  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   
  pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff      
    inet 10.255.2.100/24 scope global enp0s3    
       valid_lft forever preferred_lft forever    
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link  
``` 
  
L'interface de l'adresse est dite "scope global"  

On voit la route ajoutée à la table de routage. La route est locale :  
```
~$ ip route
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100
:~$ ping 10.255.2.1  
PING 10.255.2.1 (10.255.2.1) 56(84) bytes of data.  
64 bytes from 10.255.2.1: icmp_seq=1 ttl=255 time=0.905 ms  
64 bytes from 10.255.2.1: icmp_seq=2 ttl=255 time=0.465 ms  
64 bytes from 10.255.2.1: icmp_seq=3 ttl=255 time=0.466 ms
```  

mais ...  
`~$ ping 1.1.1.1`  
`ping: connect: Network is unreachable`  (pas de route, pas d'paquet...).

```
~$ ip -6 route   
::1 dev lo proto kernel metric 256 pref medium  
fe80::/64 dev enp0s3 proto kernel metric 256 pref medium  

 ~$ ip -4 route    
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100 `  
ip route show (plus intéressant lorsqu'on en a plusieures). 
```

On rajoute une route pour une adresse bien précise (/32):  
`sudo ip route add 1.1.1.1/32 via 10.255.2.1 dev enp0s3` 

``` 
~$ ip route
1.1.1.1 via 10.255.2.1 dev enp0s3   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100

` ~$ ping 1.1.1.1  ` 
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.  
64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=17.7 ms  
64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=29.9 ms  
64 bytes from 1.1.1.1: icmp_seq=3 ttl=54 time=11.2 ms  
64 bytes from 1.1.1.1: icmp_seq=4 ttl=54 time=14.0 ms  
  
--- 1.1.1.1 ping statistics ---  
4 packets transmitted, 4 received, 0% packet loss, time 3005ms    
rtt min/avg/max/mdev = 11.193/18.222/29.900/7.131 ms    

~$ sudo ip route add default via 10.255.2.1  dev enp0s3  
~$ ip route  
default via 10.255.2.1 dev enp0s3     
1.1.1.1 via 10.255.2.1 dev enp0s3   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100 

```

Pour une addresse n'existant pas sur le réseau. Impossible de pinger, la route merde. On veut donc s'en débarrasser :  

```
 ip route del "exemple: 8.8.8.8/32"  
~$ ip route
default via 10.255.2.1 dev enp0s3 
1.1.1.1 via 10.255.2.1 dev enp0s3                                                            
8.8.8.8 via 10.255.2.129 dev enp0s3   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100 
`~$ ping 8.8.8.8`   
```PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
8.8.8.8 ping statistics
3 packets transmitted, 0 received, 100% packet loss, time 2031ms 

```
  

```
~$ sudo ip route del 8.8.8.8 via   10.255.2.129 dev enp0s3  
~$ ip route  
default via 10.255.2.1 dev enp0s3   
1.1.1.1 via 10.255.2.1 dev enp0s3  
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100  
```  

Une autre? pour la forme ? :  
` ~$ sudo ip route del 1.1.1.1/32 via 10.255.2.1 dev enp0s3 ` 

On vérifie :   
``` ~$ ip route 
default via 10.255.2.1 dev enp0s3   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100 
```

## Dans le cas d'une erreure à corriger:  

``` 
~$ sudo ip add add 123.123.123.123/24 dev enp0s3  
~$ ip add  
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state     UNKNOWN group default qlen 1000  
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
    inet 127.0.0.1/8 scope host lo  
       valid_lft forever preferred_lft forever  
    inet6 ::1/128 scope host   
       valid_lft forever preferred_lft forever  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  
    inet 10.255.2.100/24 scope global enp0s3  
       valid_lft forever preferred_lft forever  
    inet 123.123.123.123/24 scope global enp0s3  
       valid_lft forever preferred_lft forever  
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link   
       valid_lft forever preferred_lft forever 
 ```

On verifie :   
```
~$ ip route   
default via 10.255.2.1 dev enp0s3   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100 
123.123.123.0/24 dev enp0s3 proto kernel scope link src   123.123.123.123 
```  

On supprime :   
`~$ sudo ip address del 123.123.123.123/24 dev enp0s3 ` 

```
~$ ip route  
default via 10.255.2.1 dev enp0s3   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100 
``` 

Ou alors on utilise `blackhole` (souvent utilisé pour la gestion des adresses privées).  
  
``` 
sudo ip route add blackhole 10.0.0.0/24 
~$ ip route
default via 10.255.2.1 dev enp0s3 
blackhole 10.0.0.0/24 
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100

```

Puisqu'on n'en veut plus, on supprime :   
`~$ sudo ip route del blackhole 10.0.0.0/24`


## Résolution dns de la machine: 

Même si le networkmanager n'est plus diponible, ses fichiers de
 configurations sont toujours présents et utiles:  
``` 
~$ cat /etc/resolv.conf    
#Generated by NetworkManager   
search amsterdam.lan     
nameserver 172.30.13.221    
`~$ nslookup google.be `   
Server:	172.30.13.221  
Address:	172.30.13.221#53  

Non-authoritative answer:  
Name:	google.be  
Address: 142.250.179.195  
Name:	google.be  
Address: 2a00:1450:4009:817::2003

```   


### Obtenir une addresse par dhcp (donc non manuel) :   
 
``` 
~$ sudo dhclient --help  
Usage: dhclient [-4|-6] [-SNTPRI1dvrxi] [-nw] [-p <port>] [-D LL  |LLT]    
               
[--dad-wait-time <seconds>]     
[--prefix-len-hint <length>]    
[--decline-wait-time <seconds>]    
[--address-prefix-len <length>]  
[-df duid-file] [-lf lease-file]    
[-pf pid-file] [--no-pid] [-e VAR=val]  
[-sf script-file] [interface]*  
dhclient {--version|--help|-h}  

~$ ip add   
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state   UNKNOWN group default qlen 1000  
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
    inet 127.0.0.1/8 scope host lo  
       valid_lft forever preferred_lft forever  
    inet6 ::1/128 scope host   
       valid_lft forever preferred_lft forever  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  
    inet 10.255.2.100/24 scope global enp0s3  
       valid_lft forever preferred_lft forever  
    inet 10.255.2.5/24 brd 10.255.2.255 scope global secondary   dynamic enp0s3  
       valid_lft 506sec preferred_lft 506sec  
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link   
       valid_lft forever preferred_lft forever  

~$ sudo dhclient -v   
Internet Systems Consortium DHCP Client 4.4.1  
Copyright 2004-2018 Internet Systems Consortium.  
All rights reserved.    
For info, please visit https://www.isc.org/software/dhcp/    

Listening on LPF/enp0s3/08:00:27:90:a9:1e    
Sending on   LPF/enp0s3/08:00:27:90:a9:1e    
Sending on   Socket/fallback    
DHCPREQUEST for 10.255.2.5 on enp0s3 to 255.255.255.255 port 67 
DHCPACK of 10.255.2.5 from 10.255.2.3  
RTNETLINK answers: File exists  
bound to 10.255.2.5 -- renewal in 266 seconds.  
```  

Au bout du bail, soit la machine se renouvelle, soit non et elle  se déconfigure et le serveur de bail va attribuer une autre    addresse.   

On peut forcer la machine à libérer le bail :      
```~$ sudo dhclient -r    
Killed old client process ```   


## save | restore 

```
 ~$ sudo ip address save  
[sudo] password for XXXX:  
Not sending a binary stream to stdout
:~$ sudo ip address save > network.config
~$ cat network.config   
Ce qui donne :   
"6GLv#c�       �����������Xv#c�  	
�  
�  
��  
  enp0y�  
        �Hv#c�	  
������������Hv#c�	  
@����  
'�������������  
{�:~$ } 
```

Hum...Pas très lisible, c'est du binaire, tu ne peux rien en faire. 
 
On redirige le fichier pour le rendre "lisible" :
` :~$ sudo ip address restore < network.config `   

La même chose mais proprement :      
```
~$ cd /etc/network/    
/etc/network$ ls -al `
total 40
drwxr-xr-x   7 root root  4096 Sep 15 09:11 .
drwxr-xr-x 123 root root 12288 Sep 15 12:47 ..
drwxr-xr-x   2 root root  4096 Sep  5 11:09 if-down.d
drwxr-xr-x   2 root root  4096 Sep  5 11:09 if-post-down.d
drwxr-xr-x   2 root root  4096 Sep  5 11:09 if-pre-up.d
drwxr-xr-x   2 root root  4096 Sep  5 11:09 if-up.d
-rw-r--r--   1 root root   240 Sep  5 11:13 interfaces
drwxr-xr-x   2 root root  4096 Nov  4  2020 interfaces.d

```

le fichier interface : 
``` 
cat interfaces 
`# This file describes the network interfaces available on your system
/# and how to activate them. For more information, see interfaces(5).
```
source /etc/network/interfaces.d/*(un commentaire là dessus? )

```/# The loopback network interface
auto lo
iface lo inet loopback
```

if up, down, post-down pre-up (sont appellées automatiquement quand il y a des modifications d'interfaces).   

On va profiter de ceci : ` source /etc/network/interfaces.d/* ` 
`Source` peut s'employer pour modifer des fichiers bsh, le profil.bashrc 


```
/etc/network$ ls -al /sys/class/net/
total 0
drwxr-xr-x  2 root root 0 Sep 15 09:09 .
drwxr-xr-x 52 root root 0 Sep 15 09:09 ..
lrwxrwxrwx  1 root root 0 Sep 15 09:09 enp0s3 -> ../../devices/pci0000:00/0000:00:03.0/net/enp0s3
lrwxrwxrwx  1 root root 0 Sep 15 09:09 lo -> ../../devices/virtual/net/lo 

```

Après configuration du fichier ```/etc/network/interfaces.d/ethernets  ```, l'interface n'est pas encore lancée au redémarrage de la machine. Pour activer l'interface :   
``` 
sudo ifup enp0s3  
Internet Systems Consortium DHCP Client 4.4.1     
Copyright 2004-2018 Internet Systems Consortium.    
All rights reserved.    
For info, please visit https://www.isc.org/software/dhcp/    

Listening on LPF/enp0s3/08:00:27:90:a9:1e    
Sending on   LPF/enp0s3/08:00:27:90:a9:1e    
Sending on   Socket/fallback    
Created duid "\000\001\000\001*\265\331]\010\000'\220\251\036".    
DHCPDISCOVER on enp0s3 to 255.255.255.255 port 67 interval 6    
DHCPOFFER of 10.255.2.6 from 10.255.2.3    
DHCPREQUEST for 10.255.2.6 on enp0s3 to 255.255.255.255 port 67    
DHCPACK of 10.255.2.6 from 10.255.2.3    
bound to 10.255.2.6 -- renewal in 266 seconds.
```    

Pour désactiver l'interface :    

```
 ~$ sudo ifdown enp0s3  
Killed old client process    
Internet Systems Consortium DHCP Client 4.4.1    
Copyright 2004-2018 Internet Systems Consortium.    
All rights reserved.    
For info, please visit https://www.isc.org/software/dhcp/    

Listening on LPF/enp0s3/08:00:27:90:a9:1e    
Sending on   LPF/enp0s3/08:00:27:90:a9:1e    
Sending on   Socket/fallback    
DHCPRELEASE of 10.255.2.6 on enp0s3 to 10.255.2.3 port 67    

```

Mais dans le fichier de configuration ` /etc/network/interfaces.d/ethernets` ` auto enp0s3`  active l'interface au démarrage du   service (pas de la machine).   

``` 
 ~$ sudo systemctl restart networking.service  
~$ ip add  
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state   UNKNOWN group default qlen 1000  
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
    inet 127.0.0.1/8 scope host lo  
       valid_lft forever preferred_lft forever  
    inet6 ::1/128 scope host   
       valid_lft forever preferred_lft forever  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  
    inet 10.255.2.6/24 brd 10.255.2.255 scope global dynamic   enp0s3  
       valid_lft 595sec preferred_lft 595sec  
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link   
       valid_lft forever preferred_lft forever 
``` 

`allow-hotplug enp0s3 `(pour les interfaces destinées à être branchées et débranchées).  (intéressant pour administrer du wi-fi). Voir le ` man /network/interfaces`   

Une autre config :  
` vim /etc/network/interfaces.d/ethernets  `   
(pour stations de travail et serveurs)    
```
auto enp0s3    
iface enp0s3 inet static    
		address 10.255.2.100/24  
		gateway 10.255.2.1 
``` 
		
Ce qui donne :   
``` 
~$ cat /etc/resolv.conf    
domain amsterdam.lan        
search amsterdam.lan          
nameserver 172.30.13.221   
```     
la résolution de nom fonctionne tant qu'existe le fichier     resolve.conf   

``` 
 ~$ sudo rm /etc/resolv.conf
 ~$ sudo systemctl restart networking.service 
 ~$ ip add 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state   UNKNOWN group default qlen 1000  
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
    inet 127.0.0.1/8 scope host lo  
       valid_lft forever preferred_lft forever  
    inet6 ::1/128 scope host   
       valid_lft forever preferred_lft forever  
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc   pfifo_fast state UP group default qlen 1000  
    link/ether 08:00:27:90:a9:1e brd ff:ff:ff:ff:ff:ff  
    inet 10.255.2.100/24 brd 10.255.2.255 scope global enp0s3  
       valid_lft forever preferred_lft forever  
    inet6 fe80::a00:27ff:fe90:a91e/64 scope link   
       valid_lft forever preferred_lft forever  
```

```
 :~$ ip route   
default via 10.255.2.1 dev enp0s3 onlink   
10.255.2.0/24 dev enp0s3 proto kernel scope link src 10.255.2.100  

auto enp0s3    
iface enp0s3 inet static    
		address 10.255.2.100/24  
		gateway 10.255.2.1  
		dns-nameservers 1.1.1.1 8.8.8.8 172.30.1.100

```  

Petit Bonus: j'avoue avoir eu du mal à suivre la fin du cours, tant cela devenait trop complexe à définir d'où le problème venait. J'ajoute le commentaire de Steve diffusé sur discord. Cela nous aidera à y voir plus clair à la suite. On remercie Steve pour ses heures sup'.

" Bon j'en sais plus sur l'histoire du resolv.conf de cet après-midi.

De manière générale:
- Ce fichier comme on l'a vu définit entre autre les serveurs DNS utilisés par le système
- Il est modifié par à peu près tout les service qui gèrent la configuration réseau, que ce soit NetworkManager ou le service networking ou encore dhcpcd le client dhcp etc. Ce qui n'est pas sans poser problème si plusieurs fonctionnent en même temps.
- Dés qu'on a au moins un service qui gère ce fichier, il convient de ne pas le modifier, sinon il sera écrasé par le service en question et les modifications apportées avec.

Avec l'install Debian GUI, on avait de base le NetworkManager qui vient avec son propre client DHCP, l'un comme l'autre modifient le fichier en direct.
En désactivant le NetworkManager pour passer par /etc/network/interfaces  pour gérer la config des interfaces on a donc bel et bien supprimé une source de conflit.
Par contre le client DHCP qui tournait encore en arrière plan remettait sa version du fichier de manière périodique. Malheureusement ce service ne s'arrête pas simplement quand on passe à une configuration d'adressage statique.
Après reboot, ce service était aussi arrêté ... mais la config faites dans le fichier interface avec l'instruction dns-nameservers pourtant correcte ne faisait pas son job.

La cause était en fait plutôt bête:
Lorsqu'on utilise /etc/network/interfaces pour configurer les interfaces et qu'on veut que le service networking adapte le fichier resolv.conf en fonction des paramètres passés par dns-nameservers il faut utilise le package resolvconf qui n'était pas présent de base.

J'ai vérifié sur une install basique sans GUI, bien évidemment les soucis de NetworkManager et client DHCP ne se posent pas ... mais par contre là non plus le package resolvconf n'est pas installé de base.
Un petit apt install resolvconf suivi d'un restart du service networking, et tout rentre dans l'ordre.

La conclusion est que le service networking n'est pas conçu pour modifier lui-même le fichier resolv.conf mais que la liaison s'opère via le package resolvconf qui apporte d'ailleurs quelques outils et commandes pour paramétrer la résolution DNS du système

et devinez quoi ...
C'est spécifique à Debian 😅 
Avec ubuntu server le package resolvconf est d'office installé."

```
:~$ sudo apt list | grep resolvconf
resolvconf-admin/focal 0.3-1 amd64
resolvconf/focal 1.82 all 
``` 








 




 
 

  

