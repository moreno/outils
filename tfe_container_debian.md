2 / 10 / 2022 20h20

Virtualisation | Oracle VM VirtualBox 
-----------------|-------------------------
OS invité | debian-11.5.0-amd64-netinst.iso  
Source suivie | [https://wiki.debian.org/nspawn#systemd-nspawn](https://wiki.debian.org/nspawn#systemd-nspawn) 
Objet | Installation et déploiement systemd-nspawn 


## Définition
> systemd-nspawn may be used to run a command or OS in a light-weight namespace container. In many ways it is similar to chroot, but more powerful since it fully virtualizes the file system hierarchy, as well as the process tree, the various IPC subsystems and the host and domain name. This mechanism is also similar to LXC, but is much simpler to configure and most of the necessary software is already installed on contemporary Debian systems.  (source: https://wiki.debian.org/nspawn#systemd-nspawn) 

 

## Top, après installation 
```
top - 21:19:00 up 10 min,  2 users,  load average: 0,19, 0,06, 0,02
Tâches:  87 total,   1 en cours,  86 en veille,   0 arrêté,   0 zombie
%Cpu(s):  0,0 ut,  0,2 sy,  0,0 ni, 99,2 id,  0,0 wa,  0,0 hi,  0,7 si,  0,0 st
MiB Mem :    976,4 total,    834,9 libr,     66,4 util,     75,1 tamp/cache
MiB Éch :    975,0 total,    975,0 libr,      0,0 util.    802,6 dispo Mem
 ```

## Debootstrap 

```
root@debianvm:~# apt install debootstrap 
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances... Fait
Lecture des informations d'état... Fait      
Les paquets supplémentaires suivants seront installés : 
  arch-test dirmngr gnupg gnupg-l10n gnupg-utils gpg gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm libassuan0 libksba8 libnpth0
  pinentry-curses
Paquets suggérés :
  ubuntu-archive-keyring squid-deb-proxy-client dbus-user-session pinentry-gnome3 tor parcimonie xloadimage scdaemon pinentry-doc
Les NOUVEAUX paquets suivants seront installés :
  arch-test debootstrap dirmngr gnupg gnupg-l10n gnupg-utils gpg gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm libassuan0 libksba8 libnpth0
  pinentry-curses
0 mis à jour, 16 nouvellement installés, 0 à enlever et 0 non mis à jour.
Il est nécessaire de prendre 7.754 ko dans les archives.
Après cette opération, 16,2 Mo d'espace disque supplémentaires seront utilisés.
Souhaitez-vous continuer ? [O/n] O
Réception de :1 http://deb.debian.org/debian bullseye/main amd64 arch-test all 0.17-1 [12,9 kB]
Réception de :2 http://deb.debian.org/debian bullseye/main amd64 debootstrap all 1.0.123+deb11u1 [75,9 kB]
Réception de :3 http://deb.debian.org/debian bullseye/main amd64 libassuan0 amd64 2.5.3-7.1 [50,5 kB]
Réception de :4 http://deb.debian.org/debian bullseye/main amd64 gpgconf amd64 2.2.27-2+deb11u2 [548 kB]
Réception de :5 http://deb.debian.org/debian bullseye/main amd64 libksba8 amd64 1.5.0-3 [123 kB]
Réception de :6 http://deb.debian.org/debian bullseye/main amd64 libnpth0 amd64 1.6-3 [19,0 kB]
Réception de :7 http://deb.debian.org/debian bullseye/main amd64 dirmngr amd64 2.2.27-2+deb11u2 [763 kB]
Réception de :8 http://deb.debian.org/debian bullseye/main amd64 gnupg-l10n all 2.2.27-2+deb11u2 [1.086 kB]
Réception de :9 http://deb.debian.org/debian bullseye/main amd64 gnupg-utils amd64 2.2.27-2+deb11u2 [905 kB]
Réception de :10 http://deb.debian.org/debian bullseye/main amd64 gpg amd64 2.2.27-2+deb11u2 [928 kB]
Réception de :11 http://deb.debian.org/debian bullseye/main amd64 pinentry-curses amd64 1.1.0-4 [64,9 kB]
Réception de :12 http://deb.debian.org/debian bullseye/main amd64 gpg-agent amd64 2.2.27-2+deb11u2 [669 kB]
Réception de :13 http://deb.debian.org/debian bullseye/main amd64 gpg-wks-client amd64 2.2.27-2+deb11u2 [524 kB]
Réception de :14 http://deb.debian.org/debian bullseye/main amd64 gpg-wks-server amd64 2.2.27-2+deb11u2 [516 kB]
Réception de :15 http://deb.debian.org/debian bullseye/main amd64 gpgsm amd64 2.2.27-2+deb11u2 [645 kB]
Réception de :16 http://deb.debian.org/debian bullseye/main amd64 gnupg all 2.2.27-2+deb11u2 [825 kB]
7.754 ko réceptionnés en 1s (8.207 ko/s)
Sélection du paquet arch-test précédemment désélectionné.
(Lecture de la base de données... 35143 fichiers et répertoires déjà installés.)
Préparation du dépaquetage de .../00-arch-test_0.17-1_all.deb ...
Dépaquetage de arch-test (0.17-1) ...
Sélection du paquet debootstrap précédemment désélectionné.
Préparation du dépaquetage de .../01-debootstrap_1.0.123+deb11u1_all.deb ...
Dépaquetage de debootstrap (1.0.123+deb11u1) ...
Sélection du paquet libassuan0:amd64 précédemment désélectionné.
Préparation du dépaquetage de .../02-libassuan0_2.5.3-7.1_amd64.deb ...
Dépaquetage de libassuan0:amd64 (2.5.3-7.1) ...
Sélection du paquet gpgconf précédemment désélectionné.
Préparation du dépaquetage de .../03-gpgconf_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gpgconf (2.2.27-2+deb11u2) ...
Sélection du paquet libksba8:amd64 précédemment désélectionné.
Préparation du dépaquetage de .../04-libksba8_1.5.0-3_amd64.deb ...
Dépaquetage de libksba8:amd64 (1.5.0-3) ...
Sélection du paquet libnpth0:amd64 précédemment désélectionné.
Préparation du dépaquetage de .../05-libnpth0_1.6-3_amd64.deb ...
Dépaquetage de libnpth0:amd64 (1.6-3) ...
Sélection du paquet dirmngr précédemment désélectionné.
Préparation du dépaquetage de .../06-dirmngr_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de dirmngr (2.2.27-2+deb11u2) ...
Sélection du paquet gnupg-l10n précédemment désélectionné.
Préparation du dépaquetage de .../07-gnupg-l10n_2.2.27-2+deb11u2_all.deb ...
Dépaquetage de gnupg-l10n (2.2.27-2+deb11u2) ...
Sélection du paquet gnupg-utils précédemment désélectionné.
Préparation du dépaquetage de .../08-gnupg-utils_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gnupg-utils (2.2.27-2+deb11u2) ...
Sélection du paquet gpg précédemment désélectionné.
Préparation du dépaquetage de .../09-gpg_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gpg (2.2.27-2+deb11u2) ...
Sélection du paquet pinentry-curses précédemment désélectionné.
Préparation du dépaquetage de .../10-pinentry-curses_1.1.0-4_amd64.deb ...
Dépaquetage de pinentry-curses (1.1.0-4) ...
Sélection du paquet gpg-agent précédemment désélectionné.
Préparation du dépaquetage de .../11-gpg-agent_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gpg-agent (2.2.27-2+deb11u2) ...
Sélection du paquet gpg-wks-client précédemment désélectionné.
Préparation du dépaquetage de .../12-gpg-wks-client_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gpg-wks-client (2.2.27-2+deb11u2) ...
Sélection du paquet gpg-wks-server précédemment désélectionné.
Préparation du dépaquetage de .../13-gpg-wks-server_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gpg-wks-server (2.2.27-2+deb11u2) ...
Sélection du paquet gpgsm précédemment désélectionné.
Préparation du dépaquetage de .../14-gpgsm_2.2.27-2+deb11u2_amd64.deb ...
Dépaquetage de gpgsm (2.2.27-2+deb11u2) ...
Sélection du paquet gnupg précédemment désélectionné.
Préparation du dépaquetage de .../15-gnupg_2.2.27-2+deb11u2_all.deb ...
Dépaquetage de gnupg (2.2.27-2+deb11u2) ...
Paramétrage de libksba8:amd64 (1.5.0-3) ...
Paramétrage de debootstrap (1.0.123+deb11u1) ...
Paramétrage de libnpth0:amd64 (1.6-3) ...
Paramétrage de libassuan0:amd64 (2.5.3-7.1) ...
Paramétrage de gnupg-l10n (2.2.27-2+deb11u2) ...
Paramétrage de gpgconf (2.2.27-2+deb11u2) ...
Paramétrage de arch-test (0.17-1) ...
Paramétrage de gpg (2.2.27-2+deb11u2) ...
Paramétrage de gnupg-utils (2.2.27-2+deb11u2) ...
Paramétrage de pinentry-curses (1.1.0-4) ...
Paramétrage de gpg-agent (2.2.27-2+deb11u2) ...
Created symlink /etc/systemd/user/sockets.target.wants/gpg-agent-browser.socket → /usr/lib/systemd/user/gpg-agent-browser.socket.
Created symlink /etc/systemd/user/sockets.target.wants/gpg-agent-extra.socket → /usr/lib/systemd/user/gpg-agent-extra.socket.
Created symlink /etc/systemd/user/sockets.target.wants/gpg-agent-ssh.socket → /usr/lib/systemd/user/gpg-agent-ssh.socket.
Created symlink /etc/systemd/user/sockets.target.wants/gpg-agent.socket → /usr/lib/systemd/user/gpg-agent.socket.
Paramétrage de gpgsm (2.2.27-2+deb11u2) ...
Paramétrage de dirmngr (2.2.27-2+deb11u2) ...
Created symlink /etc/systemd/user/sockets.target.wants/dirmngr.socket → /usr/lib/systemd/user/dirmngr.socket.
Paramétrage de gpg-wks-server (2.2.27-2+deb11u2) ...
Paramétrage de gpg-wks-client (2.2.27-2+deb11u2) ...
Paramétrage de gnupg (2.2.27-2+deb11u2) ...
Traitement des actions différées (« triggers ») pour man-db (2.9.4-2) ...
Traitement des actions différées (« triggers ») pour libc-bin (2.31-13+deb11u4) ...

```

## installation de systemd-container

```
moreno@debianvm:~$ sudo apt install systemd-container 
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances... Fait
Lecture des informations d'état... Fait      
Les paquets supplémentaires suivants seront installés : 
  libnss-mymachines
Les NOUVEAUX paquets suivants seront installés :
  libnss-mymachines systemd-container
0 mis à jour, 2 nouvellement installés, 0 à enlever et 0 non mis à jour.
Il est nécessaire de prendre 665 ko dans les archives.
Après cette opération, 1.680 ko d'espace disque supplémentaires seront utilisés.
Souhaitez-vous continuer ? [O/n] O
Réception de :1 http://deb.debian.org/debian bullseye/main amd64 systemd-container amd64 247.3-7+deb11u1 [441 kB]
Réception de :2 http://deb.debian.org/debian bullseye/main amd64 libnss-mymachines amd64 247.3-7+deb11u1 [224 kB]
665 ko réceptionnés en 0s (3.190 ko/s)     
Sélection du paquet systemd-container précédemment désélectionné.
(Lecture de la base de données... 35514 fichiers et répertoires déjà installés.)
Préparation du dépaquetage de .../systemd-container_247.3-7+deb11u1_amd64.deb ...
Dépaquetage de systemd-container (247.3-7+deb11u1) ...
Sélection du paquet libnss-mymachines:amd64 précédemment désélectionné.
Préparation du dépaquetage de .../libnss-mymachines_247.3-7+deb11u1_amd64.deb ...
Dépaquetage de libnss-mymachines:amd64 (247.3-7+deb11u1) ...
Paramétrage de systemd-container (247.3-7+deb11u1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/machines.target → /lib/systemd/system/machines.target.
Paramétrage de libnss-mymachines:amd64 (247.3-7+deb11u1) ...
First installation detected...
Checking NSS setup...
Traitement des actions différées (« triggers ») pour libc-bin (2.31-13+deb11u4) ...
Traitement des actions différées (« triggers ») pour man-db (2.9.4-2) ...
Traitement des actions différées (« triggers ») pour dbus (1.12.20-2) ...
```

## TODO

```
moreno@debianvm:~$ sudo su
root@debianvm:/home/moreno# echo 'kernel.unprivileged_userns_clone=1' >/etc/sysctl.d/nspawn.conf
```

## Restart et status de systemd-sysctl.service

```
root@debianvm:/home/moreno# systemctl restart systemd-sysctl.service
root@debianvm:/home/moreno# systemctl status systemd-sysctl.service
● systemd-sysctl.service - Apply Kernel Variables
     Loaded: loaded (/lib/systemd/system/systemd-sysctl.service; static)
     Active: active (exited) since Sun 2022-10-02 14:29:16 CEST; 7s ago
       Docs: man:systemd-sysctl.service(8)
             man:sysctl.d(5)
    Process: 4528 ExecStart=/lib/systemd/systemd-sysctl (code=exited, status=0/SUCCESS)
   Main PID: 4528 (code=exited, status=0/SUCCESS)
        CPU: 5ms

oct 02 14:29:16 debianvm systemd[1]: Stopped Apply Kernel Variables.
oct 02 14:29:16 debianvm systemd[1]: Stopping Apply Kernel Variables...
oct 02 14:29:16 debianvm systemd[1]: Starting Apply Kernel Variables...
oct 02 14:29:16 debianvm systemd[1]: Finished Apply Kernel Variables.
root@debianvm:/home/moreno#
```

```
oct 02 14:26:28 debianvm sudo[4337]:   moreno : TTY=pts/0 ; PWD=/home/moreno ; USER=root ; COMMAND=/usr/bin/apt install systemd-container
oct 02 14:26:28 debianvm sudo[4337]: pam_unix(sudo:session): session opened for user root(uid=0) by moreno(uid=1000)
oct 02 14:26:33 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:33 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:33 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:33 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:34 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:35 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:35 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:36 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:36 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:36 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:36 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:36 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:41 debianvm systemd[1]: Reloading.
oct 02 14:26:45 debianvm dbus-daemon[337]: [system] Reloaded configuration
oct 02 14:26:46 debianvm sudo[4337]: pam_unix(sudo:session): session closed for user root
oct 02 14:28:28 debianvm sudo[4521]:   moreno : TTY=pts/0 ; PWD=/home/moreno ; USER=root ; COMMAND=/usr/bin/su
oct 02 14:28:28 debianvm sudo[4521]: pam_unix(sudo:session): session opened for user root(uid=0) by moreno(uid=1000)
oct 02 14:28:28 debianvm su[4522]: (to root) moreno on pts/0
oct 02 14:28:28 debianvm su[4522]: pam_unix(su:session): session opened for user root(uid=0) by moreno(uid=0)
oct 02 14:29:16 debianvm systemd[1]: systemd-sysctl.service: Succeeded.
oct 02 14:29:16 debianvm systemd[1]: Stopped Apply Kernel Variables.
oct 02 14:29:16 debianvm systemd[1]: Stopping Apply Kernel Variables...
oct 02 14:29:16 debianvm systemd[1]: Starting Apply Kernel Variables...
oct 02 14:29:16 debianvm systemd[1]: Finished Apply Kernel Variables.
```

## Création du container debian 

``` 
debootstrap --include=systemd-container stable /var/lib/machines/debian 
```

## Se logger au container nouvellement créé et permettre le login de root :  

``` 
moreno@debianvm:~$ sudo systemd-nspawn -D /var/lib/machines/debian -U --machine debian
Spawning container debian on /var/lib/machines/debian.
Press ^] three times within 1s to kill container.
Selected user namespace base 1766326272 and range 65536.
root@debian:~# passwd
New password: 
Retype new password: 
passwd: password updated successfully
root@debian:~# 
```
  
 ## Permettre le login via local tty ...et s'en aller...  

```
root@debian:~# echo 'pts/0' >> /etc/securetty                                  
root@debian:~# hostname debiancont
root@debian:~# ip add
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:21:83:9e brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.196/24 brd 192.168.0.255 scope global dynamic enp0s3
       valid_lft 82324sec preferred_lft 82324sec
    inet6 2a02:2788:54:be3:a00:27ff:fe21:839e/64 scope global dynamic mngtmpaddr 
       valid_lft 300sec preferred_lft 300sec
    inet6 fe80::a00:27ff:fe21:839e/64 scope link 
       valid_lft forever preferred_lft forever
root@debian:~# logout

Container debian exited successfully.
```

## Start et status de systemd-nspawn

```
moreno@debianvm:~$ sudo systemctl start systemd-nspawn@debian
moreno@debianvm:~$ sudo systemctl status systemd-nspawn@debian
● systemd-nspawn@debian.service - Container debian
     Loaded: loaded (/lib/systemd/system/systemd-nspawn@.service; disabled; ven>
     Active: active (running) since Sun 2022-10-02 18:07:35 CEST; 10s ago
       Docs: man:systemd-nspawn(1)
   Main PID: 4391 (systemd-nspawn)
     Status: "Container running: Startup finished in 2.632s."
      Tasks: 11 (limit: 16384)
     Memory: 23.2M
        CPU: 709ms
     CGroup: /machine.slice/systemd-nspawn@debian.service
             ├─payload
             │ ├─init.scope
             │ │ └─4393 /usr/lib/systemd/systemd
             │ └─system.slice
             │   ├─console-getty.service
             │   │ └─4440 /sbin/agetty -o -p -- \u --noclear --keep-baud consol>
             │   ├─cron.service
             │   │ └─4435 /usr/sbin/cron -f
             │   ├─dbus.service
             │   │ └─4436 /usr/bin/dbus-daemon --system --address=systemd: --no>
             │   ├─rsyslog.service
             │   │ └─4437 /usr/sbin/rsyslogd -n -iNONE
             │   ├─systemd-journald.service
             │   │ └─4412 /lib/systemd/systemd-journald
             │   └─systemd-logind.service
             │     └─4438 /lib/systemd/systemd-logind
             └─supervisor
               └─4391 systemd-nspawn --quiet --keep-unit --boot --link-journal=>

oct 02 18:07:37 debianvm systemd-nspawn[4391]: [  OK  ] Reached target Login Pr>
oct 02 18:07:38 debianvm systemd-nspawn[4391]: [  OK  ] Started System Logging >
oct 02 18:07:38 debianvm systemd-nspawn[4391]: [  OK  ] Started User Login Mana>
oct 02 18:07:38 debianvm systemd-nspawn[4391]: [  OK  ] Reached target Multi-Us>
oct 02 18:07:38 debianvm systemd-nspawn[4391]: [  OK  ] Reached target Graphica>
oct 02 18:07:38 debianvm systemd-nspawn[4391]:          Starting Update UTMP ab>
oct 02 18:07:38 debianvm systemd-nspawn[4391]: [  OK  ] Finished Update UTMP ab>
oct 02 18:07:39 debianvm systemd-nspawn[4391]: 
oct 02 18:07:39 debianvm systemd-nspawn[4391]: Debian GNU/Linux 11 debian conso>
oct 02 18:07:39 debianvm systemd-nspawn[4391]: 
lines 17-39/39 (END)
             │   ├─cron.service
             │   │ └─4435 /usr/sbin/cron -f
             │   ├─dbus.service
             │   │ └─4436 /usr/bin/dbus-daemon --system --address=systemd: --nof>
             │   ├─rsyslog.service
             │   │ └─4437 /usr/sbin/rsyslogd -n -iNONE
             │   ├─systemd-journald.service
             │   │ └─4412 /lib/systemd/systemd-journald
             │   └─systemd-logind.service
             │     └─4438 /lib/systemd/systemd-logind
             └─supervisor
               └─4391 systemd-nspawn --quiet --keep-unit --boot --link-journal=t>
```

## Processus et parentalité avec htop 

TODO


## Liste de la/des machine.s 
```
moreno@debianvm:~$ machinectl list
MACHINE CLASS     SERVICE        OS     VERSION ADDRESSES
debian  container systemd-nspawn debian 11      -        
```

## Se logger via machinectl
```
moreno@debianvm:~$ sudo machinectl login debian 
[sudo] Mot de passe de moreno : 
Connected to machine debian. Press ^] three times within 1s to exit session.

Debian GNU/Linux 11 debian pts/1

debian login: root
Password: 
Linux debian 5.10.0-18-amd64 #1 SMP Debian 5.10.140-1 (2022-09-02) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
```

## Adressage ip 
```
root@debian:~# ip a 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: host0@if3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 4a:a6:60:92:91:ab brd ff:ff:ff:ff:ff:ff link-netnsid 0
```