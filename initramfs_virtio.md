# CentOS sur KVM

j'ai procédé à l'installation de centos sur virtualbox dans le cadre de mon travail de fin de formation. Bien que nous travaillons principalement sur Debian et Ubuntu, il s'agissait d'en prendre la température, ce qui justifiait ce choix. 

Mes machines physiques sont toutes deux des debian 11. J'essaie d'y installer une vm centos sur kvm. J'ai donc besoin de Virtio.

Notes : Pour le clavier en fr si nécessaire :    
```https://www.it-connect.fr/passer-le-clavier-en-azerty-sur-centos-7-rhel-7/ ```

 
Ci-dessous, on regarde si le kernel supporte le driver de Virtio, c'est à dire que l'on regarde si CONFIG_VIRTIO_BLK, CONFIG_SCSI_VIRTIO et CONFIG_VIRTIO_NET sont "=m". Si oui, suite à la commande: 
` grep -i virtio /boot/config-$(uname -r)  `     
   
``` CONFIG_BLK_MQ_VIRTIO=y    
CONFIG_VIRTIO_VSOCKETS=m  
CONFIG_VIRTIO_VSOCKETS_COMMON=m  
CONFIG_NET_9P_VIRTIO=m  
CONFIG_VIRTIO_BLK=m  
CONFIG_SCSI_VIRTIO=m  
CONFIG_VIRTIO_NET=m  
CONFIG_VIRTIO_CONSOLE=m  
CONFIG_HW_RANDOM_VIRTIO=m  
CONFIG_DRM_VIRTIO_GPU=m  
CONFIG_VIRTIO=m  
CONFIG_VIRTIO_MENU=y  
CONFIG_VIRTIO_PCI=m  
CONFIG_VIRTIO_PCI_LEGACY=y  
CONFIG_VIRTIO_PMEM=m  
CONFIG_VIRTIO_BALLOON=m  
CONFIG_VIRTIO_MEM=m   
CONFIG_VIRTIO_INPUT=m  
CONFIG_VIRTIO_MMIO=m  
# CONFIG_VIRTIO_MMIO_CMDLINE_DEVICES is not set  
CONFIG_VIRTIO_DMA_SHARED_BUFFER=m  
# CONFIG_RPMSG_VIRTIO is not set  
CONFIG_VIRTIO_FS=m  
CONFIG_CRYPTO_DEV_VIRTIO=m  
```
  
On regarde si virtio est inclus dans le `temporary file system`. Si lsinitrd n'est pas reconnue, il manquerait `dracut`. Initramfs sera remplacé. On comprend qu'il faut donc faire un choix. Ici, de la doc à ce sujet : 
``` 
https://command-not-found.com/lsinitrd 
https://endocs.ksyun.com/documents/5425
```

L'installation de dracut supprimera initramfs-tool. Cela semble risqué. Est-ce le cas ?     

``` 
https://wiki.debian.org/initramfs-tools  
https://packages.qa.debian.org/d/dracut.html
```

Après un reboot, surprise, ma machine centos sur KVM fonctionne mais n'est pas visible. Network default n'est pas démarré. Donc...
```
 https://wiki.debian.org/KVM#Libvirt_default_network 
```
me conseillant de réaliser les deux commandes :   

``` 
virsh --connect=qemu:///system net-start default 
```   
et     
```
virsh --connect=qemu:///system net-autostart default
```  

```https://wiki.debian.org/KVM#Between_VM_host_and_guests```

Trois fichiers restent à être supprimés. Une fois cela fait, c'est bon. Centos est là, visible, tout roule. 