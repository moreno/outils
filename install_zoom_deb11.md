## Installation ZOOM Debian 11

``` 
source : https://www.how2shout.com/linux/how-to-install-zoom-client-on-debian-11-bullseye-linux/    
auteur : Heyan Maurya   
date : 2021 
``` 

## 1. Open Terminal & Run system update:  
```
sudo apt update  
```

## 2. Download the Zoom Meeting Linux app:  
``` 
wget https://zoom.us/client/latest/zoom_amd64.deb  
```

## 3. Command to install Zoom on Debian 11 or 10
(When it asks to Restart services, select Yes and hit the Enter key):    
``` 
sudo apt install ./zoom_amd64.deb 
```    

## 4. Launch Zoom Linux client  :
Than, execute : ``` zoom ```  

And if it's necessary, Uninstall :     
``` 
sudo apt remove zoom  
```

 "Et voilà !"







